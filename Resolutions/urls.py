from django.conf.urls import url
from . import views

app_name='Resolutions'


urlpatterns = [
	# /resolutions/
    url(r'^$', views.index, name='index'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login_user/$', views.login_user, name='login_user'),
    url(r'^logout_user/$', views.logout_user, name='logout_user'),
	url(r'^(?P<resolution_id>[0-9]+)/$', views.detail, name='detail'),
    #url(r'^create_resolution/$', views.create_resolution, name='create_resolution'),
    #url(r'resolution/(?P<pk>[0-9]+)/$', views.ResolutionUpdate.as_view(), name='resolution-update'),
    url(r'(?P<resolution_id>[0-9]+)/delete_resolution/$', views.delete_resolution, name='delete_resolution'),
]

	
