from django.contrib.auth.models import Permission, User
from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.
class Resolution(models.Model):
	user = models.ForeignKey(User, default= 1)
	resolutionTitle = models.CharField(max_length = 50)

	def __str__(self):
		return self.resolutionTitle

class resolDescription(models.Model):
	resolution = models.ForeignKey(Resolution, on_delete=models.CASCADE) 
	resolutionDescription = models.CharField(max_length = 300)

	def __str__(self):
		return self.resolutionDescription