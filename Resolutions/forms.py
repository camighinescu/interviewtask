from django.contrib.auth.models import User
from django import forms
from django.forms.extras.widgets import SelectDateWidget
import datetime
from .models import Resolution, resolDescription


class ResolutionForm(forms.ModelForm):

    class Meta:
        model = Resolution
        fields = ['resolutionTitle']

class DescriptionForm(forms.ModelForm):

    class Meta:
        model = resolDescription
        fields = ['resolutionDescription']




class UserForm(forms.ModelForm):
	username = forms.CharField(max_length=30, required=True, help_text='Optional.')
	firstName = forms.CharField(max_length=30, required=False, help_text='Optional.')
	surName = forms.CharField(max_length=30, required=False, help_text='Optional.')
	password = forms.CharField(widget=forms.PasswordInput)
	email = forms.EmailField(max_length=254, help_text='Required. Inform a valid email address.')
	dob = forms.DateField(label=('dob'), widget=SelectDateWidget(years=range(2018,1900,-1)))

	class Meta:
		model = User
		fields = ['username','firstName', 'surName','email', 'dob', 'password']

