from django.contrib import admin
from .models import Resolution, resolDescription

admin.site.register(Resolution)
admin.site.register(resolDescription)