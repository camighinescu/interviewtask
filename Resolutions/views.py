from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.views import generic
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse_lazy
from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from django.db.models import Q
from .models import Resolution, resolDescription
from .forms import UserForm, ResolutionForm


def index(request):
    if not request.user.is_authenticated():
        return render(request, 'Resolutions/login.html')
    else:
        resolutions = Resolution.objects.filter(user=request.user)
        description_results = resolDescription.objects.all()
        query = request.GET.get('q')
        if query:
            resolutions = resolutions.filter(
                Q(resolution_title__icontains=query)
                ).distinct()
            description_results = description_results.filter(
                Q(description_results__icontains=query)
                ).distinct()
            return render(request, 'Resolutions/index.html', {
                'resolutions': resolution,
                'description_results': description_results

            })
        else:
            return render(request, 'Resolutions/index.html', {'resolutions': resolutions})

'''def create_resolution(request):
    if not request.user.is_authenticated():
        return render(request, 'Resolutions/login.html')
    else:
        form = ResolutionForm(request.POST or None)
        if form.is_valid():
            resolution = form.save(commit=False)
            resolution.user = request.user
            resolution.resolutionTitle = resolution.cleaned_data['resolutionTitle']
            resolution.save()
            return render(request, 'Resolutions/detail.html', {'resolution': resolution})
        context = {
            'form': form,
        }
        return render(request, 'Resolutions/create_resolution.html', context)
'''
def delete_resolution(request, resolution_id):
    resolution = Resolution.objects.get(pk=resolution_id)
    resolution.delete()
    resolution = Resolution.objects.filter(user=request.user)
    return render(request, 'Resolutions/index.html', {'resolution': resolution})

def detail(request, resolution_id):
    if not request.user.is_authenticated():
        return render(request, 'Resolutions/login.html')
    else:
        user = request.user
        resolution = get_object_or_404(Resolution, pk=resolution_id)
        return render(request, 'Resolutions/detail.html', {'resolution': resolution, 'user': user})

def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'Resolutions/login.html', context)

def login_user(request):
    if request.method == "POST":
        username = request.POST["username"]
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                resolutions = Resolution.objects.filter(user=request.user)
                return render(request, 'resolutions/index.html', {'resolutions': resolutions})
            else:
                return render(request, 'resolutions/login.html', {'error_message': 'Your account has been disabled'})
        else:
            return render(request, 'resolutions/login.html', {'error_message': 'Invalid login'})
    return render(request, 'resolutions/login.html')



def register(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        firstName = form.cleaned_data['firstName']
        surName = form.cleaned_data['surName']
        email = form.cleaned_data['email']
        dob = form.cleaned_data['dob']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=email, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                resolution = Resolution.objects.filter(user=request.user)
                return render(request, 'Resolutions/index.html', {'resolutions': resolutions})
    context = {
        "form": form,
    }
    return render(request, 'Resolutions/register.html', context)
